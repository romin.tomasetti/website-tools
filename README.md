[missing badges]

# website-tools

This repository is meant to become a set of useful tools for people developing websites.

To do: list language and frameworks that are targeted.

## Test and Deploy

All tests are descibed in [link to CI].

## Usage

To be integrated as a submodule in other projects.

## Contributing

Contributions are accepted. Open a MR.

## Authors and acknowledgment

* Tomasetti Romin [link to profile]

## License

Licensing to be done.

## Project status

Beta version.
