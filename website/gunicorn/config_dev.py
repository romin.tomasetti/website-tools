"""
Gunicorn *development* configuration file.
Useful for starting new development.
"""

import os

# Use this variable to save Gunicorn-related files in a given directory
GUNICORN_ROOT_DIR = os.environ.get('GUNICORN_ROOT_DIR','')

# Django WSGI application path in pattern MODULE_NAME:VARIABLE_NAME
wsgi_app = "website.wsgi:application"
chdir    = "website"

# The granularity of Error log outputs
loglevel = "debug"

# The number of worker processes for handling requests
workers = 2

# The socket to bind
bind = "0.0.0.0:8000"

# Restart workers when code changes (development only!)
reload = True

# Write access and error info to /var/log or stdout
# https://docs.gunicorn.org/en/latest/settings.html?highlight=wsgi_app#accesslog
tofile = False
if tofile:
    accesslog = errorlog = f"{GUNICORN_ROOT_DIR}/var/log/gunicorn/dev.log"
    os.makedirs(os.path.dirname(accesslog),exist_ok=True)
else:
    accesslog= errorlog = "-"

# Redirect stdout/stderr to log file
capture_output = True

# PID file so you can easily fetch process ID
pidfile = f"{GUNICORN_ROOT_DIR}/var/run/gunicorn/dev.pid"
os.makedirs(os.path.dirname(pidfile),exist_ok=True)

# Daemonize the Gunicorn process (detach & enter background)
daemon = False
