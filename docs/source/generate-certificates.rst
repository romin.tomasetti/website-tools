Generate certificates with Let's Encrypt
----------------------------------------

SSL certificates can be generated with :file:`security/https/generate_certificates.py`.
You will need a `docker compose` configuration with at least one `nginx` and one `certbot`
service.

References:
    * https://pentacent.medium.com/nginx-and-lets-encrypt-with-docker-in-less-than-5-minutes-b4b8a60d3a71

Testing the generator script
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Testing could be done using (Let's Encrypt Pebble)[https://github.com/letsencrypt/pebble#docker].

However, it was too complicated to setup in a way that would really be meaningful.

References:
    * https://blog.xoxzo.com/2020/11/18/root-certificates-generation-using-acme-server-pebble/
