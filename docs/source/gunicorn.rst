gunicorn
========

`gunicorn` is a `Python WSGI HTTP Server for UNIX`.

It is usually used in conjunction with `Django` to make a production-ready server, because the `Django`
web server is supposed to be used for development only.

`gunicorn` will also need a *reverse-proxy* like `nginx`.

Useful resources:
    * https://docs.gunicorn.org/en/stable/index.html
    * https://realpython.com/django-nginx-gunicorn
