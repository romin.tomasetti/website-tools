"""
Inspired by:
    * https://raw.githubusercontent.com/wmnnd/nginx-certbot/master/init-letsencrypt.sh
"""
import argparse
import logging
from pathlib import Path
import subprocess
import typing

import typeguard

def parse_args():
    """
    Parse CLI arguments.
    """
    parser = argparse.ArgumentParser(description = "Generate certificates using Let's Encrypt")

    parser.add_argument(
        "--cert-dir",
        type = str, required = False,
        default = "/etc/letsencrypt/",
        help = "Certificates directory",
    )
    parser.add_argument(
        "--domains",
        type = str, required = True,
        help = "Domain(s) for the certificate, e.g. 'example.com'",
    )

    parser.add_argument(
        "--email",
        type = str, required = True,
        help = "Email, e.g. 'test@example.com'",
    )

    parser.add_argument(
        "--rsa-key-size",
        type = int, required = False,
        default = 4096,
        help = "RSA key size",
    )

    parser.add_argument(
        "--staging",
        action  = 'store_true',
        help    = "Set when testing, see also https://letsencrypt.org/docs/staging-environment/"
    )

    parser.add_argument(
        "--docker-compose-files",
        type = str, required = True,
    )

    return parser.parse_args()

class DockerComposeRun(object):
    @typeguard.typechecked
    def __init__(self, docker_compose_files : typing.List[str], cmd : typing.List[typing.Union[str,Path]], type : typing.List[str] = ["up"], down : bool = True) -> None:
        self.docker_compose_file = docker_compose_files
        self.cmd                 = cmd
        self.type                = type
        self.down                = down
    def __enter__(self) -> None:
        cmd = ["docker","compose"] + self.docker_compose_file + self.type + self.cmd
        logging.info(f"> Running {cmd}")
        subprocess.check_call(cmd)
    def __exit__(self, type, value, traceback) -> None:
        logging.info(f"{type}, {value}, {traceback}")
        if self.down is True:
            cmd = ["docker","compose"] + self.docker_compose_file + ["down"]
            logging.info(f"> Running {cmd}")
            subprocess.check_call(cmd)
        else:
            logging.info("> Not calling down")

@typeguard.typechecked
def generate_certificates(*, args : argparse.Namespace):
    """
    Generate certificates using Let's Encrypt.
    """
    # Change arguments to their true type
    args.domains              = args.domains.split(" ")
    args.docker_compose_files = [x for sublist in args.docker_compose_files.split(" ") for x in ['--file',sublist]]

    logging.info(
        f"""
        > Generating HTTPS certificates with:
            * cert. dir.            : {args.cert_dir}
            * domain(s)             : {args.domains}
            * email                 : {args.email}
            * RSA key size          : {args.rsa_key_size}
            * staging               : {args.staging}
            * docker-compose file(s): {args.docker_compose_files}
        """)

    # Certificate files
    files_dir = {
        'live'    : Path(args.cert_dir) / 'live'    / args.domains[0],
        'archive' : Path(args.cert_dir) / 'archive' / args.domains[0],
        'renewal' : Path(args.cert_dir) / 'renewal' / args.domains[0],
    }
    file_key  = files_dir['live'] / "privkey.pem"
    file_cert = files_dir['live'] / "fullchain.pem"
    logging.info(f"> Certificate files will be {file_key} and {file_cert} and files directories are {files_dir}")

    # Creating dummy certificates for nginx to be able to start
    logging.info("> Creating dummy certificates")
    cmd_1 = [
        "--entrypoint=",
        "certbot","mkdir","-p",str(files_dir['live'])
    ]
    cmd_2 = [
        "--entrypoint=",
        "certbot",
        "openssl","req","-x509","-nodes","-newkey",f"rsa:{args.rsa_key_size}",
        "-days","1",
        "-keyout",str(file_key),
        "-out"   ,str(file_cert),
        "-subj","/CN=localhost/C=BE/L=Brussels"
    ]
    with DockerComposeRun(docker_compose_files=args.docker_compose_files, type = ["run","--rm"], cmd = cmd_1, down = False):
        pass
    with DockerComposeRun(docker_compose_files=args.docker_compose_files, type = ["run","--rm"], cmd = cmd_2, down = False):
        pass

    # Start nginx
    logging.info("> Starting nginx in detached mode (with dummy certificates)")
    with DockerComposeRun(docker_compose_files=args.docker_compose_files,cmd = ["--force-recreate","-d","nginx"]) as nginx:

        # Deleting dummy certificates
        logging.info("> Deleting dummy certificates with certbot")
        with DockerComposeRun(
            docker_compose_files=args.docker_compose_files,
            type = ["run","--rm"],
            cmd = [
                "--entrypoint=","certbot","rm","-rf",
                str(files_dir['archive'].parent),
                str(files_dir['live'   ].parent),
                str(files_dir['renewal'].parent),
            ],
            down = False
        ) as certbot:
            pass

        # Creating true certificates with Let's Encrypt
        # See also:
        #   * https://eff-certbot.readthedocs.io/en/stable/using.html#certbot-command-line-options
        cmd = [
            "certbot","certonly",
            "--noninteractive",
            "--rsa-key-size",str(args.rsa_key_size),
            "--webroot","-w","/var/www/certbot",
            "--agree-tos","--force-renewal",
            "-vv"
        ]
        for d in args.domains:
            cmd += ["-d",d]
        if not args.email:
            cmd += ["--register-unsafely-without-email"]
        else:
            cmd += ["--email",args.email]
        if args.staging:
            cmd.append("--staging")

        logging.info(f"> Generating certificates with Let's Encrypt using {cmd}")
        with DockerComposeRun(docker_compose_files=args.docker_compose_files, type = ["run","--rm"], cmd = cmd, down = False):
            pass

if __name__ == "__main__":

    # Configure logging
    logging.basicConfig(level = logging.INFO)

    # Parse arguments
    args = parse_args()

    # Generate certificate
    generate_certificates(args = args)
